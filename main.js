let cvs
let ctx
const direction = { up: '⬆', right: '➡', down: '⬇', left: '⬅' }
let robotPosition = { row: 0, col: 0 }
let robotDirection = direction.down

window.addEventListener('load', (event) => {
    cvs = document.querySelector('canvas')
    cvs.height = cvs.width = 500
    ctx = cvs.getContext('2d')
    window.requestAnimationFrame(think)
})

let prevTS = -1

function randomTurn() {
    const r = Math.random()

    if (r < .25) {
        robotDirection = direction.down
    } else if (r < .5) {
        robotDirection = direction.right
    } else if (r < .75) {
        robotDirection = direction.left
    } else {
        robotDirection = direction.up
    }
}

function moveForward() {
    switch (robotDirection) {
        case direction.down:
            if (robotPosition.row !== 9) {
                ++robotPosition.row
            }
            break;
        case direction.up:
            if (robotPosition.row !== 0) {
                --robotPosition.row
            }
            break;
        case direction.left:
            if (robotPosition.col !== 0) {
                --robotPosition.col
            }
            break;
        case direction.right:
            if (robotPosition.col !== 9) {
                ++robotPosition.col
            }
            break;
        default:
            break;
    }
}

function think(timestamp) {
    if (prevTS < timestamp - 100) {
        prevTS = timestamp

        const r = Math.random()

        if (r < .64) {
            moveForward()
        } else {
            randomTurn()
        }

        drawRobot()
    }
    window.requestAnimationFrame(think)
}



function drawRobot() {
    ctx.clearRect(0, 0, 500, 500)

    ctx.fillStyle = 'red'
    ctx.fillRect(0, 0, 500, 500)
    const blockSize = 500 / 10

    ctx.strokeStyle = 'black'
    for (let i = 1; i < 10; ++i) {
        ctx.beginPath()
        ctx.moveTo(blockSize * i, 0)
        ctx.lineTo(blockSize * i, 500)
        ctx.stroke()
        ctx.beginPath()
        ctx.moveTo(0, blockSize * i)
        ctx.lineTo(500, blockSize * i)
        ctx.stroke()
    }

    const robotPosX = blockSize * robotPosition.col + blockSize / 2
    const robotPosY = blockSize * robotPosition.row + blockSize / 2

    ctx.textAlign = 'center'
    ctx.textBaseline = 'middle'
    ctx.font = "48px serif";
    ctx.fillStyle = 'black'
    ctx.fillText(robotDirection, robotPosX, robotPosY)
}